# Configuraciones

## Settings

### Normas

- Mantener lo mas organizado posible con secciones por medio de comentarios
- Tener organizado en varios archivos segun la regla definida mas abajo
- Ver variaciones necesarias y convertirlas en variables de entorno

### Archivos

Mantener una carpeta `settings` con los archivos:

- **__init__.py**: para que sea un modulo de python
- **base.py**: con todas las configuraciones que se usan tanto en producción como desarrollo
- **dev.py**: primera linea utiliza todo lo de `base.py` con `from .base import *` y despues las configuraciones unicamente usadas en desarrollo
- **prod.py**: primera linea utiliza todo lo de `base.py` con `from .base import *` y despues las configuraciones unicamente usadas en desarrollo

### ¿Como utilizarlo?

En el archivo `manage.py` vamos a encontrar una linea `os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'mysite.settings.dev')` en donde el segundo parametro de `setdefault` es la dirección del setting default y `DJANGO_SETTINGS_MODULE` es el nombre de una variable de entorno que se puede configurar para utlizar otro settings

#### Ejemplos

Para un determinado entorno se podria configurar una variable de entorno:

- **Prod**: `DJANGO_SETTINGS_MODULE=mysite.settings.prod`
- **Test**: `DJANGO_SETTINGS_MODULE=mysite.settings.test` (tendria que existir un `settings/test.py` con configuración especifica de testing)
- Cualquier otro se podria crear y configurar (por ejemplo un staging o sandbox)****

## Requirements

### Normas

- No agregar dependencias que no sean propias
- No usar `pip frezze`, ya que retornar las dependencias de las dependencias tambien
- Tenerlo siempre al dia
- Cada vez que se instale una depencia nueva agregarla

### Archivos

Mantener una carpeta `requirements` con los archivos:

- **base.txt**: con todas las dependecias que se usan tanto en producción como desarrollo
- **dev.txt**: primera linea instala lo de `base.txt` con `-r base.txt` y despues las dependencias unicamente usadas en desarrollo
- **prod.txt**: primera linea instala lo de `base.txt` con `-r base.txt` y despues las dependencias unicamente usadas en producción

### Proceso de nueva dependencia

- Instalar: `pip install example`
- Buscar version: `pip freeze | grep example`
- Agregar al archivo txt la linea de `example==1.0.0`
- Ordenar el archivo para facilitar la lectura

## Ejemplo de front

En la carpeta `front` hay tres archivos de ejemplos en js de como interactuar con la api:

- **check_user.html**: Logear al usuario con su mail y password y hacer un get authenticado a un endpoint de rest framework
- **register_user.html**: Registrar un nuevo usuario y verificar el mail
- **reset_password.html**: Resetear la contraseña de un usuario

## Linting

Proximamente

## Formatter

Proximamente
