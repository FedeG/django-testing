import pytest
from django.test import Client, TestCase
from django.urls import reverse
from rest_framework import status
from rest_framework.exceptions import ErrorDetail


@pytest.mark.django_db
class ApiQuestionsTest(TestCase):
    fixtures = ['users', 'questions', 'choices']

    def setUpTestData():
        # Create db objects from ORM
        pass

    def setUp(self):
        self.client = Client()
        self.client_logged = Client()
        self.client_logged.login(username='admin', password='secret')
        self.api_url = reverse('question-list')

    def test_questions_with_not_logged_client(self):
        """
        context (setUp)

        action (objeto a testear)

        assert (verificacion)
        """

        # Issue a GET request.
        response = self.client.get(self.api_url)

        # Check that the response is 403 OK.
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertDictEqual(
            response.data,
            {
                'detail': ErrorDetail(
                    string='Authentication credentials were not provided.',
                    code='not_authenticated',
                )
            },
        )

    def test_questions_with_logged_client(self):
        # Issue a GET request.
        response = self.client_logged.get(self.api_url)

        # Check that the response is 200 OK.
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)
        self.assertDictEqual(
            response.data[0],
            {
                'id': 1,
                'question_text': 'Test',
                'pub_date': '2022-08-30T18:58:55Z',
            },
        )
