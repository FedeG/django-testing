import pytest
from django.urls import reverse
from rest_framework import status
from rest_framework.exceptions import ErrorDetail

from api.tests.factories import QuestionFactory


@pytest.mark.django_db
def test_questions_with_not_logged_client(client_unlogged):
    # Get urls
    api_url = reverse('question-list')

    # Issue a GET request.
    response = client_unlogged.get(api_url)

    # Check that the response is 403 OK.
    assert response.status_code == status.HTTP_403_FORBIDDEN
    assert response.data == {
        'detail': ErrorDetail(
            string='Authentication credentials were not provided.',
            code='not_authenticated',
        )
    }


@pytest.mark.django_db
def test_questions_with_logged_client(client_logged):
    # SetUp
    question = QuestionFactory()

    # Get urls
    api_url = reverse('question-list')

    # Issue a GET request.
    response = client_logged.get(api_url)

    # Check that the response is 200 OK.
    assert response.status_code == status.HTTP_200_OK
    assert len(response.data) == 1
    assert response.data[0]['id'] == question.id
