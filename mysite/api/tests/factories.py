import factory
from django.contrib.auth.models import User
from django.db.models.signals import post_save

from polls.models import Choice, Question


@factory.django.mute_signals(post_save)
class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = User

    email = factory.Faker('email')
    username = factory.Faker('user_name')
    password = factory.PostGenerationMethodCall('set_password', 'default_password')


class QuestionFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Question

    pub_date = factory.Faker('date_time_between')
    question_text = factory.Faker('sentence', nb_words=6)


class ChoiceFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Choice

    question = factory.SubFactory(QuestionFactory)
    choice_text = factory.Faker('word')
