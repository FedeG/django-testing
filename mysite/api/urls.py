from django.urls import path, include
from rest_framework import routers

from .views import ChoiceViewSet, QuestionViewSet

router = routers.DefaultRouter()
router.register(r'choices', ChoiceViewSet)
router.register(r'questions', QuestionViewSet)

urlpatterns = [
    path('auth/', include('rest_framework.urls')),
    path('', include(router.urls)),
]
