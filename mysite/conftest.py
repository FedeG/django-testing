import pytest

from django.contrib.auth.models import User

from rest_framework.test import APIClient


class LoggedAPIClient(APIClient):
    user_logged: User


@pytest.fixture
def client_logged(django_user_model) -> LoggedAPIClient:
    user = django_user_model.objects.create_user(
        username='regular_user', password='secret'
    )
    client = LoggedAPIClient()
    client.login(username='regular_user', password='secret')
    client.user_logged = user
    return client


@pytest.fixture
def admin_client_logged(django_user_model) -> LoggedAPIClient:
    user = django_user_model.objects.create_user(
        username='admin', password='secret', staff=True, is_superuser=True
    )
    client = LoggedAPIClient()
    client.login(username='admin', password='secret')
    client.user_logged = user
    return client


@pytest.fixture
def unlogged_user(django_user_model) -> User:
    return django_user_model.objects.create_user(
        username='unlogged_user', password='secret'
    )


@pytest.fixture
def client_unlogged(unlogged_user) -> APIClient:
    client = APIClient()
    client.unlogged_user = unlogged_user
    return client
