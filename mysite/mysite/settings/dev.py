from .base import *  # noqa

ALLOWED_HOSTS = ['*']
DEBUG = True
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

# CORSHEADERS
CORS_ALLOW_ALL_ORIGINS = True
