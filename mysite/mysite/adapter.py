from allauth.account.adapter import DefaultAccountAdapter
from django.conf import settings


class DefaultAccountAdapterCustom(DefaultAccountAdapter):
    def send_mail(self, template_prefix, email, context):

        if 'email_confirmation_signup' in template_prefix:
            key = context['key']
            context['activate_url'] = f'{settings.URL_FRONT}/verify-email/{key}'

        if 'password_reset_key' in template_prefix:
            default_password_reset_url = context['password_reset_url']
            token = default_password_reset_url.split('/')[-2]
            uid = default_password_reset_url.split('/')[-3]
            context['token'] = token
            context['uid'] = uid
            context[
                'password_reset_url'
            ] = f'{settings.URL_FRONT}/confirm-reset-password/{uid}/{token}'

        msg = self.render_mail(template_prefix, email, context)
        msg.send()
