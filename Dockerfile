FROM python:3.10

WORKDIR /usr/src/app

RUN apt-get update && apt-get install -y gettext && apt-get clean && rm -rf /var/lib/apt/lists/*

COPY ./requirements/ /usr/src/app/requirements/
RUN pip install --quiet --upgrade pip
RUN pip install --quiet -r requirements/dev.txt

COPY . /usr/src/app/
