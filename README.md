# my site django testing

## Install pre-commit

```bash
pre-commit install
```

## Get reports for linting and coverage

```bash
# Create flake8 report on public folder
flake8 --format=html --htmldir=public --exit-zero mysite/

# Create pytest coverage report on htmlcov folder
pytest mysite/ -n auto --cov-config=.coveragerc --cov-report term --cov-report html --no-cov-on-fail --cov=mysite/.
```

## Setup desarrollo

Python>=3.10

### Crear un entorno virtual

```bash
python -m virtualenv env
```

### Activar el entorno

```bash
source env/bin/activate
```

### instalar dependencias

```bash
# For production environment
pip install -r requirements/base.txt

# For development environment
pip install -r requirements/dev.txt
```

### Correr migraciones

```bash
python manage.py migrate
```

### Correr proyecto

```bash
python manage.py runserver
```

### Ejecutar los ejemplos de front

```bash
cd front
python3 -m http.server 9000
```
